

/* Includes ------------------------------------------------------------------*/
#include "lwip/opt.h"
#include "lwip/ip.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/icmp.h"
#include "fs.h"
#include "string.h"
#include "http.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "lan8742.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define XMLCLIENT_THREAD_PRIO    ( tskIDLE_PRIORITY + 5 )

#ifndef PING_DATA_SIZE
#define PING_DATA_SIZE 32
#endif

#ifndef PING_ID
#define PING_ID        0xAFAF
#endif

#if LWIP_DHCP
#define MAX_DHCP_TRIES  4
__IO uint8_t DHCP_state = DHCP_OFF;
#endif

#define LAST_TRANSFER_TIMEOUT 20

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u32_t nPageHits = 0;



uint8_t rx_buffer[100];
uint8_t rx_rssi_buffer[100];

uint8_t* getRSSIBuffer(void){
	return(rx_rssi_buffer);
}

typedef enum 
{
  LoginPage = 0,
  FileUploadPage,
  UploadDonePage,
  ResetDonePage
}htmlpageState;

htmlpageState htmlpage;

extern __IO int32_t PHYLinkState;

//char netconn_send_data_buff[1000];
__IO uint8_t isDataToSend = 0;
extern uint8_t rx_buffer[];
extern uint8_t rx_rssi_buffer[];
__IO uint8_t lastTransfer = LAST_TRANSFER_TIMEOUT;


char www_page_buffer[600];

void TestDynWebPageCreate(uint8_t* data);				


void http_client_serve (struct netconn *conn); 

//void http_test_thread(void *arg);
//void xml_client_sender_netconn_thread(void *arg);
//void xml_client_netconn_thread(void *arg);

uint8_t* getRSSIBuffer(void);



extern SemaphoreHandle_t semaphoreDataSync;

extern IWDG_HandleTypeDef hiwdg1;

void http_netconn_thread(void *arg)
{ 
	//HAL_IWDG_Refresh(&hiwdg1);
	vTaskDelay(1000);
	//HAL_IWDG_Refresh(&hiwdg1);
  struct netconn *conn, *newconn;
  ip_addr_t local_ipaddr, remote_ipaddr;
  err_t err = 0xFF;
	struct netbuf *inbuf;
	char* buf;
  u16_t buflen;

	IP4_ADDR(&local_ipaddr, IP_ADDR0 ,IP_ADDR1 , IP_ADDR2 , IP_ADDR3); 
	IP4_ADDR(&remote_ipaddr, SERVER_IP_ADDR0 ,SERVER_IP_ADDR1 , SERVER_IP_ADDR2 , SERVER_IP_ADDR3);

	if(PHYLinkState > LAN8742_STATUS_LINK_DOWN){
	  
						conn = netconn_new(NETCONN_TCP); 
						err = netconn_bind(conn, &local_ipaddr, 80);
						err = netconn_listen(conn);
	
	          while(1){
						if(conn==NULL || err!=ERR_OK){
				
										//netconn_close(conn);
										netconn_delete(conn);
										vTaskDelay(100);
										conn = netconn_new(NETCONN_TCP); 
										err = netconn_bind(conn, &local_ipaddr, 80);
										err = netconn_listen(conn);
										} else {
	      
									DSIG_RGB_A_GREEN(1);
									err = netconn_accept(conn,&newconn);
									if(err == ERR_OK)
									{
										err = netconn_recv(newconn,&inbuf);
										if(err == ERR_OK){
										netbuf_data(inbuf, (void**)&buf, &buflen);
										strcpy(www_page_buffer, "HTTP/1.0 200 OK\r\nServer: Scadvance-D-Luczak\r\nAccess-Control-Allow-Origin: *\r\nContent-type: text/html\r\n\r\n");
										err = netconn_write(newconn, (const unsigned char*)www_page_buffer, strlen(www_page_buffer), NETCONN_NOCOPY);	
									/*	if (strncmp(buf, "GET ", 4))
										{
											if (strstr(buf, "index.php") !=NULL) {
												strcpy(www_page_buffer, "HTTP/1.1\r\nHost: 192.168.34.21\r\n\r\nHello!\r\n\r\n");
												err = netconn_write(newconn, (const unsigned char*)www_page_buffer, strlen(www_page_buffer), NETCONN_NOCOPY);	
											}
											else 
											{
												strcpy(www_page_buffer, "HTTP/1.1\r\nHost: 192.168.34.21\r\n\r\nError 404\r\n\r\n");
												err = netconn_write(newconn, (const unsigned char*)www_page_buffer, strlen(www_page_buffer), NETCONN_NOCOPY);	
											}
										}*/
											
										} 
									  netbuf_free(inbuf);
										netconn_close(newconn);										
										
								}
								netconn_delete(newconn);
								DSIG_RGB_A_GREEN(0);
							}	
						}
			
	} 
			
}


void HTTP_MakeHexStringFromUINT8(uint8_t id, char* pcText)
{
	int8_t index = 0;
	uint8_t uValue = id;
	char cValue;
	for(index=1; index>=0; index--)
	{
		cValue = (char)(uValue&0x0F);
		if(cValue>9){*(pcText+index)=cValue+0x37;}else{*(pcText+index)=cValue+0x30;}
		uValue>>=4;
	}
	*(pcText + 2) = '\0';
}


void TestDynWebPageCreate(uint8_t* rssi_data)
{
	uint8_t index;	
	strcpy(www_page_buffer,"GET /RS485?rawData=");
	for(index=0;index<90;index++){
		HTTP_MakeHexStringFromUINT8(*(rssi_data+index),	(www_page_buffer+19+(index<<1)));
  }
	strcat(www_page_buffer," HTTP/1.1\r\nHost: 192.168.34.21\r\nConnection: close\r\n\r\nHello!");
}	

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Notify the User about the nework interface config status 
  * @param  netif: the network interface
  * @retval None
  */
void ethernet_link_status_updated(struct netif *netif) 
{
  if (netif_is_up(netif))
 {
#if LWIP_DHCP
    /* Update DHCP state machine */
    DHCP_state = DHCP_START;
#elif defined(USE_LCD)
    uint8_t iptxt[20];
    sprintf((char *)iptxt, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
    LCD_UsrLog ("Static IP address: %s\n", iptxt);
#else
   
#endif /* LWIP_DHCP */
  }
  else
  {  
#if LWIP_DHCP
    /* Update DHCP state machine */
    DHCP_state = DHCP_LINK_DOWN;
#elif defined(USE_LCD)
    LCD_UsrLog ("The network cable is not connected \n"); 
#else

#endif /* LWIP_DHCP */
  } 
}

#if LWIP_DHCP
/**
  * @brief  DHCP Process
  * @param  argument: network interface
  * @retval None
  */
void DHCP_Thread(void const * argument)
{
  struct netif *netif = (struct netif *) argument;
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;
  struct dhcp *dhcp;
#ifdef USE_LCD  
  uint8_t iptxt[20];
#endif
  
  for (;;)
  {
    switch (DHCP_state)
    {
    case DHCP_START:
      {
        ip_addr_set_zero_ip4(&netif->ip_addr);
        ip_addr_set_zero_ip4(&netif->netmask);
        ip_addr_set_zero_ip4(&netif->gw);    
        DHCP_state = DHCP_WAIT_ADDRESS;
#ifdef USE_LCD  
        LCD_UsrLog ("  State: Looking for DHCP server ...\n");
#else
        BSP_LED_Off(LED1);
        BSP_LED_Off(LED2);
#endif
        dhcp_start(netif);
      }
      break;    
    case DHCP_WAIT_ADDRESS:
      {                
        if (dhcp_supplied_address(netif)) 
        {
          DHCP_state = DHCP_ADDRESS_ASSIGNED;	
         
#ifdef USE_LCD 
          sprintf((char *)iptxt, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));  
          LCD_UsrLog ("IP address assigned by a DHCP server: %s\n", iptxt);
#else
          BSP_LED_On(LED1);
          BSP_LED_Off(LED2);
#endif
        }
        else
        {
          dhcp = (struct dhcp *)netif_get_client_data(netif, LWIP_NETIF_CLIENT_DATA_INDEX_DHCP);
    
          /* DHCP timeout */
          if (dhcp->tries > MAX_DHCP_TRIES)
          {
            DHCP_state = DHCP_TIMEOUT;
            
            /* Stop DHCP */
            dhcp_stop(netif);
            
            /* Static address used */
            IP_ADDR4(&ipaddr, IP_ADDR0 ,IP_ADDR1 , IP_ADDR2 , IP_ADDR3 );
            IP_ADDR4(&netmask, NETMASK_ADDR0, NETMASK_ADDR1, NETMASK_ADDR2, NETMASK_ADDR3);
            IP_ADDR4(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);
            netif_set_addr(netif, ip_2_ip4(&ipaddr), ip_2_ip4(&netmask), ip_2_ip4(&gw));
             
#ifdef USE_LCD  
            sprintf((char *)iptxt, "%s", ip4addr_ntoa(netif_ip4_addr(netif)));
            LCD_UsrLog ("DHCP Timeout !! \n");
            LCD_UsrLog ("Static IP address: %s\n", iptxt); 
#else
            BSP_LED_On(LED1);
            BSP_LED_Off(LED2);
#endif 
          }
        }
      }
      break;
  case DHCP_LINK_DOWN:
    {
      /* Stop DHCP */
      dhcp_stop(netif);
      DHCP_state = DHCP_OFF;
#ifdef USE_LCD  
      LCD_UsrLog ("The network cable is not connected \n"); 
#else
      BSP_LED_Off(LED1);
      BSP_LED_On(LED2);
#endif
    }
    break;
    default: break;
    }
    
    /* wait 500 ms */
    osDelay(500);
  }
}
#endif  /* LWIP_DHCP */

