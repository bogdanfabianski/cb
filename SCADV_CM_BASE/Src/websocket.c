/**
 ******************************************************************************
  * File Name          : websocket.c
  * Description        : This file provides code for the configuration
  *                      of the mbedtls instances.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "websocket.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN 2 */
extern __IO int32_t PHYLinkState;
char net_buffer[50];
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
/**
 * Send WebSocket.
 */
err_t websocket_send(struct netconn *conn, const void *dataptr, uint8_t opcode, uint8_t Mask, uint32_t MaskKey, uint32_t len)
{
	return 0;
}
/**
 * Receive WebSocket.
 */
err_t websocket_recv(struct netconn *conn, const void *dataptr, uint32_t *len)
{
	return 0;
}
/**
 * Connect WebSocket.
 * TODO mbedtls_net_recv with timeout
 * TODO ws key weryfication
 * TODO define errors
 */
struct netconn* websocket_connect(err_t *err, ip_addr_t* local_ipaddr, ip_addr_t* remote_ipaddr, uint16_t remote_port, uint16_t local_port, char *localkey, char *remotekey)
{
	struct netconn *newconn;
	char ws_buffer[300];
	char *temp;
	uint16_t buflen;
	uint8_t ip1,ip2,ip3,ip4;
	
	ip1=(uint32_t)(remote_ipaddr->addr>>24);
	ip2=(uint32_t)(remote_ipaddr->addr>>16);
	ip3=(uint32_t)(remote_ipaddr->addr>>8);
	ip4=(uint8_t)remote_ipaddr->addr;
	
	sprintf(ws_buffer,"GET / HTTP/1.1\r\nHost: %d.%d.%d.%d:%d\r\n",ip4,ip3,ip2,ip1,remote_port);
	strcat(ws_buffer,"Upgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: ");
	strcat(ws_buffer,localkey);
	strcat(ws_buffer,"\r\nSec-WebSocket-Extensions: permessage-deflate; client_max_window_bits\r\n\r\n");
	/* connect to server at ip: remote_ipaddr and port: 80*/
	newconn = net_connect(local_ipaddr, remote_ipaddr, remote_port, local_port);
	if(newconn!=NULL)
	{
		*err = net_send(newconn, ws_buffer,15, NETCONN_NOCOPY);
		*err = net_recv(newconn, ws_buffer, &buflen);
		if(*err != ERR_OK)
		{
			netconn_close(newconn);
			netconn_delete(newconn);
			*err=ERR_CONN;
			return NULL;
		}
		if(strstr(ws_buffer,"HTTP/1.1 101 Switching Protocols")!=NULL)
		{
			if(strstr(ws_buffer,"Upgrade: WebSocket")!=NULL)
			{
				if(strstr(ws_buffer,"Connection: Upgrade")!=NULL)
				{
					temp=strstr(ws_buffer,"Sec-WebSocket-Accept:")+22;
					if(temp!=NULL)
					{
						strncpy(remotekey,temp,28);
						*(remotekey+28)=0x00;
						
						return newconn;
					}
				}
			}
		}
	}
	netconn_close(newconn);
	netconn_delete(newconn);
	*err=ERR_CONN;
	return NULL;
}
/**
 * Echo TCP client thread.
 *
 * @note This function will try to connect server at ip_addr_t remote_ipaddr.
 */
void websocket_net_thread(void * pvParameters)
{
	vTaskDelay(5000);
  struct netconn *conn;
  ip_addr_t local_ipaddr, remote_ipaddr;
	uint16_t buflen;
  err_t err;
	
	char localkey[25]="bKdPyn3u98cTfZJSh4TNeQ==";
	char remotekey[25];

  IP4_ADDR(&local_ipaddr, IP_ADDR0 ,IP_ADDR1 , IP_ADDR2 , IP_ADDR3); 
  IP4_ADDR(&remote_ipaddr, SERVER_IP_ADDR0 ,SERVER_IP_ADDR1 , SERVER_IP_ADDR2 , SERVER_IP_ADDR3);
	/* infinite loop of a task */
	while(1) 
	{
		if(PHYLinkState > LAN8742_STATUS_LINK_DOWN)
		{
			/* connect to server at ip: remote_ipaddr and port: 80*/
			conn = websocket_connect(&err, &local_ipaddr, &remote_ipaddr, 80, 0, localkey, remotekey);
			if(conn!=NULL)
			{
				do
				{
					/* send net_buffer and wait for answer */
					err = net_send(conn, net_buffer,15, NETCONN_NOCOPY);
					err = net_recv(conn, net_buffer, &buflen);
				}while(err == ERR_OK);
				/* if connection lost close and delete conn */
				netconn_close(conn);
				netconn_delete(conn);
			}
		}
		vTaskDelay(2000);
	}
}
/* USER CODE END 4 */

/**
  * @}
  */
 
/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
