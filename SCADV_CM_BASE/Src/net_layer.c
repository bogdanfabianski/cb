/**
 ******************************************************************************
  * File Name          : net_layer.c
  * Description        : This file provides code for the network layer
  *                      of the probe.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "net_layer.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN 2 */
extern __IO int32_t PHYLinkState;
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
/**
 * Initialize local IP adress, port and initiate a TCP connection witch remote server.
 *
 * @param local_ipaddr the local IP address to bind the netconn to
 * @param remote_ipaddr the remote IP address to bind the netconn to
 * @param remote_port the remote port to bind the netconn to
 * @param local_port the local port to bind the netconn to
 * @return struct netconn* if connected
 */
struct netconn* net_connect(ip_addr_t* local_ipaddr, ip_addr_t* remote_ipaddr, uint16_t remote_port, uint16_t local_port)
{	
	struct netconn *conn;
	err_t err;
	/* setup the PCB */
	conn = netconn_new(NETCONN_TCP); 
	err = netconn_bind(conn, local_ipaddr, local_port);
	if(conn!=NULL && err==ERR_OK)
	{
		/* connect to server at ip: remote_ipaddr and port: 80*/
		err = netconn_connect(conn, remote_ipaddr, remote_port);
	}
	return conn;
}
/**
 * Send data over a TCP netconn.
 *
 * @param conn the TCP netconn over which to send data
 * @param dataptr pointer to the application buffer that contains the data to send
 * @param apiflags combination of following flags :
 * - NETCONN_COPY: data will be copied into memory belonging to the stack
 * - NETCONN_MORE: for TCP connection, PSH flag will be set on last segment sent
 * - NETCONN_DONTBLOCK: only write the data if all data can be written at once
 * @return ERR_OK if data was sent, any other err_t on error
 */
err_t net_send(struct netconn *conn, const void *dataptr, uint32_t size, uint8_t apiflags)
{
	err_t err;
	err = netconn_write(conn, (const unsigned char*)dataptr, size, apiflags);
	return err;
}
/**
 * Receive data from a netconn.
 *
 * @param conn the TCP netconn over which to send data
 * @param dataptr pointer to the application buffer where received data is contained
 * @param len pointer to the variable which stores the lenght of received data
 * @return ERR_OK if data was sent, any other err_t on error
 */
err_t net_recv(struct netconn *conn, void *dataptr, uint16_t *len)
{
	struct netbuf *inbuf;
	err_t err;
	
	err = netconn_recv(conn, &inbuf);
	if (err == ERR_OK)
	{
		/* copy received buffer of length: buflen to net_buffer */
		*len = netbuf_len( inbuf );
		netbuf_copy(inbuf, (uint8_t*)dataptr, *len ); 
	}
	/* deallocate the netbuf */
	netbuf_delete(inbuf);
	return err;
}
/* USER CODE END 4 */

/**
  * @}
  */
 
/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
