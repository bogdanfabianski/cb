#ifndef _VARIABLE_H_
#define _VARIABLE_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Variable
*/
#include "variable-type-basic.h"


/**
 * Find variable by id
 * @param[in] id Id of variable
 * @param[in] pStart Starting pointer to memory
 * @param[in] pEnd End address of structure memory
 * @return Pointer to variable. Return 0 if not found.
 */
variable_t* variable_find_by_id(uint32_t id, variable_t* pStart, uint32_t pEnd);

/**
 * Set pointed variable value
 * @param[in] pVariable Pointer to variable
 * @param[in] value_string String containing value to be set
  */
void variable_set_value(variable_t* pVariable, char* value_string);


/**
 * Set pointed variable alias
 * @param[in] pVariable Pointer to variable
 * @param[in] alias Alias of variable
  */
void variable_set_alias(variable_t* pVariable, char* alias);

/**
 * Set pointed variable access level
 * @param[in] pVariable Pointer to variable
 * @param[in] level Access level of variable
  */
void variable_set_access_level(variable_t* pVariable, uint8_t level);

/**
 * Get pointer to next variable
 * @param[in] pStart Pointer to variable
 * @return Pointer to next variable.
 */
variable_t* variable_get_next_variable(variable_t* pVariable);

#endif
