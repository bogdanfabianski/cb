#ifndef _STATUS_TYPE_H_
#define _STATUS_TYPE_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Status type
*/
#include "status-type-common.h"
	
//List of status

typedef struct{

  uint32_t  struct_version;	///< Parameter version fo structure - changed by programmer
  uint32_t	count;	///< Number of parameters
  uint32_t	size;	///< Size of parameters struct

  struct{
		//Central board
	  struct {	  
			status_float32_t temperature;	  
	  }central_s;
	  
		//Peripherial board
	  struct {
			status_float32_t temperature;
	  }peripheral_s;
  }list_s;
	
}status_t; 	///< Parameters structure

#endif
