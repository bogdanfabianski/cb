/**
*	@author Dominik Luczak
*	@date 2018-09-28
*	@brief HTTP-request handler based on https://github.com/pellepl/verisure1512/blob/master/ext-libs/STM32Cube_FW_F7_V1.2.0/Projects/STM32756G_EVAL/Applications/LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/httpserver-netconn.c
*/

#include "lwip/api.h"
#include "restful.h"
#include "json-variable.h"
#include "parameter.h"
#include "../hardware-layer/unique-id.h"
#include "firmware-version.h"

#define check_request(request,key) !strncmp((char *)request, key, sizeof(key)-1 )
#define WEBSERVER_THREAD_PRIO ( tskIDLE_PRIORITY + 2 )
void DynWebPage(struct netconn *conn);

void http_server_serve(struct netconn *connection){

struct netbuf *inbuf;
err_t recv_err;  
uint16_t buflen;
char* read_data;
char* recv_buffer;
uint32_t id=0;
	
  /* Read the data from the port, blocking if nothing yet there. 
   We assume the request (the part we care about) is in one netbuf */
  recv_err = netconn_recv(connection, &inbuf);	

  if (recv_err == ERR_OK)
  {
    if (netconn_err(connection) == ERR_OK) 
    {
		netbuf_data(inbuf, (void**)&recv_buffer, &buflen);
		read_data=recv_buffer;  
  
	/* Is this an HTTP GET command? (only check the first 5 chars, since
      there are other formats for GET, and we're keeping it very simple )*/
      if ((buflen >=5) && (strncmp(recv_buffer, "GET /", 5) == 0))
      {	
				recv_buffer += strlen("GET /"); //skip "GET /"
				read_data=recv_buffer;

				if(check_request(recv_buffer, "index.php")){	
					
				}
				else if(check_request(recv_buffer, "parameters.php")){					//Parameters full structure JSON				
					restful_get_parameter_structure(connection, json_variable_full_e, HTTP_SEND_HEADER_YES);
				}
				else if( check_request(recv_buffer, "parameter_get.php?")){		//Get parameter by ID	?id=XXXX									
					read_data += strlen("parameter_get.php?");	//get ID
					sscanf((char *)read_data,"id=%X",&id);
					restful_get_parameter_by_id(connection , id);
				}
				else if(check_request(recv_buffer, "parameter_set.php?")){			//Set parameter by ID ?id=XXXX&v=XXX				
					read_data += strlen("parameter_set.php?");	//get ID
					sscanf(read_data,"id=%X",&id);
					read_data = strchr(read_data,'v');
					read_data = strchr(read_data,'=')+1;			
					restful_set_parameter_value_by_id(connection ,id, read_data); 
				}
				else if(check_request(recv_buffer, "status.php")){					//Parameters full structure JSON				
					restful_get_status_structure(connection, json_variable_full_e, HTTP_SEND_HEADER_YES);
				}				
				else if(check_request(recv_buffer, "info.php"))
        {
           /* Load dynamic page */
           DynWebPage(connection);
				}
				else
				{
					netconn_write(connection, "Page not found", strlen("Page not found") , NETCONN_COPY);
				}
	  }
	}
  }
  
  /* Close the connection (server closes in HTTP) */
  netconn_close(connection);
  
  /* Delete the buffer (netconn_recv gives us ownership,
   so we have to make sure to deallocate the buffer) */
	netbuf_delete(inbuf);  
}



/**
  * @brief  http server thread 
  * @param arg: pointer on argument(not used here) 
  * @retval None
  */
static void http_server_netconn_thread(void *arg)
{ 
  struct netconn *conn, *newconn;
  err_t err, accept_err;
  
	vTaskDelay(2000);
	
  /* Create a new TCP connection handle */
  conn = netconn_new(NETCONN_TCP);
  
  if (conn!= NULL)
  {
    /* Bind to port 80 (HTTP) with default IP address */
    err = netconn_bind(conn, NULL, 80);
    
    if (err == ERR_OK)
    {
      /* Put the connection into LISTEN state */
      netconn_listen(conn);
  
      while(1) 
      {
        /* accept any icoming connection */
        accept_err = netconn_accept(conn, &newconn);
        if(accept_err == ERR_OK)
        {
          /* serve connection */
          http_server_serve(newconn);
					
          /* delete connection */
          netconn_delete(newconn);
        }
      }
    }
  }
}

/**
  * @brief  Initialize the HTTP server (start its thread) 
  * @param  none
  * @retval None
  */
void http_server_netconn_init(void)
{
	xTaskCreate(http_server_netconn_thread, "HTTP", configMINIMAL_STACK_SIZE*5, NULL, WEBSERVER_THREAD_PRIO , NULL);
}

/**
  * @brief  Create and send a dynamic Web Page. This page contains the list of 
  *         running tasks and the number of page hits. 
  * @param  conn pointer on connection structure 
  * @retval None
  */
void DynWebPage(struct netconn *conn)
{
  portCHAR PAGE_BODY[1024];
  portCHAR pagehits[10] = {0};

	static uint32_t nPageHits = 0;
	
  memset(PAGE_BODY, 0,1024);

  /* Update the hit count */
  nPageHits++;
  sprintf(pagehits, "%d", (int)nPageHits);
  strcat(PAGE_BODY, pagehits);
	
	strcat((char *)PAGE_BODY, "\n ID: " );
	strcat((char *)PAGE_BODY,  unique_id_text );
	strcat((char *)PAGE_BODY, "\n" );
	
	strcat((char *)PAGE_BODY, "\n Firmware version: " );
	strcat((char *)PAGE_BODY,  firmware_version );
	strcat((char *)PAGE_BODY, "\n" );
	
	strcat((char *)PAGE_BODY, "\n Firmware build time: " );
	strcat((char *)PAGE_BODY,  firmware_build_time );
	strcat((char *)PAGE_BODY, "\n" );
	strcat((char *)PAGE_BODY, "\n" );
	
	 
//  strcat((char *)PAGE_BODY, "<pre><br>Name          State  Priority  Stack   Num" );
//  strcat((char *)PAGE_BODY, "<br>---------------------------------------------<br>");
//    
//  /* The list of tasks and their status */
//  osThreadList((unsigned char *)(PAGE_BODY + strlen(PAGE_BODY)));
//  strcat((char *)PAGE_BODY, "<br><br>---------------------------------------------");
//  strcat((char *)PAGE_BODY, "<br>B : Blocked, R : Ready, D : Deleted, S : Suspended<br>");

  /* Send the dynamically generated page */
  netconn_write(conn, PAGE_BODY, strlen(PAGE_BODY), NETCONN_COPY);
}


