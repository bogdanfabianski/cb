#ifndef _STATUS_TYPE_BASIC_H_
#define _STATUS_TYPE_BASIC_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Status type
*/
#include "variable-type-basic.h"
#include "arm_math.h" //float32, float64
	
//Default type of parameter
typedef variable_t status_single_t;

typedef variable_uint8_t 	status_uint8_t;
typedef variable_uint16_t 	status_uint16_t;
typedef variable_uint32_t 	status_uint32_t;
typedef variable_float32_t 	status_float32_t;
typedef variable_bool_t 	status_bool_t;
typedef variable_select_t 	status_select_t;


#endif
