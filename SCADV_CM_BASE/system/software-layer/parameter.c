/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameter.h"
#include "variable.h"

parameters_t parameters;

/**
 * Get size of parameter list
 * @return size of parameter list
 */
uint32_t parameter_get_size_of_list(void);


parameter_t* parameter_find_by_id(uint32_t id){
	
	parameter_t* pParameter=0;	
	variable_t* pStart = (variable_t*) parameter_get_fisrt();
	uint32_t end_address = (uint32_t) parameter_get_fisrt();
	end_address += parameter_get_size_of_list();
	
	pParameter = (parameter_t*) variable_find_by_id( id, pStart, end_address);
	
	return pParameter;
}

parameter_t* parameter_set_value_by_id(uint32_t id, char* value_string)
{
	parameter_t* pParameter=0;	
	pParameter = parameter_find_by_id(id);
	
	if(pParameter)
	variable_set_value( (variable_t*) pParameter, value_string);
	
	return pParameter;
}

parameter_t* parameter_get_fisrt(void)
{
	return (parameter_t*) &parameters.list_s;
}


parameter_t* parameter_get_next_parameter(parameter_t* pParameter)
{	
	uint32_t end_address = (uint32_t) parameter_get_fisrt();
	end_address += parameter_get_size_of_list();
	parameter_t* pParameter_next=0;
	
	pParameter_next = (parameter_t*)variable_get_next_variable((variable_t*) pParameter);	
	if((uint32_t)pParameter_next > end_address ) pParameter_next=NULL; // NULL - out of memory range
	
	return pParameter_next;
}


//Private function

uint32_t parameter_get_size_of_list(void)
{
	return sizeof(parameters.list_s);
}



