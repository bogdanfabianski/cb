#ifndef _VARIABLE_TYPE_H_
#define _VARIABLE_TYPE_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Variable type
*/
#include <stdint.h>	//uint8_t uint16_t
#include "arm_math.h" //float32, float64

#define VARIABLE_ALIAS_LENGTH_DEF 31

enum variable_type_t{								///< List of variable types
	variable_uint8_e=1,
	variable_uint16_e,
	variable_uint32_e,
	variable_int8_e,
	variable_int16_e,
	variable_int32_e,
	variable_float32_e,
	variable_bool_e,
	variable_select_e
};

//Variable parts
	//Variable const part of settings
	typedef struct {
		uint16_t	id;
		uint8_t		type;
		char* 		name;
		char* 		description;
	}variable_const_t;

	//Variable var part of settings
	typedef struct __attribute__((packed)){
		uint8_t access_level;
		char alias[VARIABLE_ALIAS_LENGTH_DEF];		
	}variable_var_t;


	typedef struct __attribute__((packed)){
		variable_var_t var_s;
		variable_const_t* pConst_s;
	}variable_settings_t;

	//General variable type
	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;			///< Common part of all types	
		uint32_t	 value;						///< Only used for properly union overlap
	}variable_t;



//Basic types	
	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		uint8_t value;						
	}variable_uint8_t;							///< uint8 type

	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		uint16_t value;						
	}variable_uint16_t;							///< uint16 type

	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		uint32_t value;						
	}variable_uint32_t;							///< uint32 type	
	
	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		float32_t value;							
	}variable_float32_t;							///< float32 type

	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		uint8_t	value;						
	}variable_bool_t;								///< bool type

	typedef struct __attribute__((packed)){
		variable_settings_t settings_s;					
		uint8_t	 value;								
	}variable_select_t;							///< select type	
	
#endif
