#ifndef _STATUS_H_
#define _STATUS_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Status
*/
#include "status-type.h"

extern status_t status;

/**
 * Find status by id
 * @param[in] id Id of status
 * @return Pointer to status. Return 0 if not found.
 */
status_t* status_find_by_id(uint32_t id);

/**
 * Get pointer to first status
 * @return Pointer to first status
 */
status_t* status_get_fisrt(void);


/**
 * Get pointer to next status
 * @param[in] pStatus Pointer to status
 * @return Pointer to next status. 0 if end.
 */
status_t* status_get_next_status(status_t* pStatus);

#endif
