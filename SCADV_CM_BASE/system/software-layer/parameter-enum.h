#ifndef _PARAMETER_ENUM_H_
#define _PARAMETER_ENUM_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameter enums
*/

enum peripheral_interface_t{
	peripheral_interface_disable_e=0,
	peripheral_interface_ethernet_e,
	peripheral_interface_can_e,
	peripheral_interface_rs485_e,
	peripheral_interface_rs232_e
};

#endif
