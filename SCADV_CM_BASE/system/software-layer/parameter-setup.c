/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameter.h"
#include "parameter-enum.h"
#include "access-level.h"

#define parameter_initialization(identifier, parameter_name, parameter_id, parameter_value, parameter_type, parameter_access_level, parameter_name_text, parameter_description_text) \
	parameters.count++;	\
	static const variable_const_t _setup_parameter_struct_ ## identifier ## _ = { parameter_id, parameter_type, parameter_name_text, parameter_description_text }; \
	parameter_name.settings_s.pConst_s = (variable_const_t* )&_setup_parameter_struct_ ## identifier ## _ ; \
	parameter_name.value 	= parameter_value; \
	parameter_name.settings_s.var_s.access_level 	= parameter_access_level;



void parameter_setup(void)
{
	//Parameters initialization	
	parameters.struct_version 	= 0x00001;	
	parameters.size 			= sizeof(parameters_t);
	parameters.count 			= 0;
	
	//CENTRAL
	parameter_initialization(CENTRAL_LOCAL_NAME, 		parameters.list_s.central_s.configuration_s.local_name,	0x0001, 	0,	variable_bool_e,  level_develop_e,	"configuration.local-name", 					"Local name - custom identifier by alias");
	
	//IP address
	parameter_initialization(CENTRAL_SYSTEM_IP_A1, 		parameters.list_s.central_s.network_s.local_s.ip.A1,		0x0001, 	192,	variable_uint8_e, level_develop_e,	"network.configuration.IP.A1", 					"Static IP address x.168.0.101, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_IP_A2, 		parameters.list_s.central_s.network_s.local_s.ip.A2,		0x0002, 	168,	variable_uint8_e, level_develop_e,	"network.configuration.IP.A2", 					"Static IP address 192.x.0.101, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_IP_A3, 		parameters.list_s.central_s.network_s.local_s.ip.A3,		0x0003, 	0,	    variable_uint8_e, level_develop_e,	"network.configuration.IP.A3", 					"Static IP address 192.168.x.101, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_IP_A4, 		parameters.list_s.central_s.network_s.local_s.ip.A4,		0x0004, 	101,	variable_uint8_e, level_develop_e,	"network.configuration.IP.A4", 					"Static IP address 192.168.0.x, x=<0;255>. Save and restart system to apply.");
	
	//MAC address
	parameter_initialization(CENTRAL_SYSTEM_MAC_A1, 		parameters.list_s.central_s.network_s.local_s.mac.A1,		0x0005, 	0x8C,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A1", 				"MAC address x:A2:A3:A4:A5:A6, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_MAC_A2, 		parameters.list_s.central_s.network_s.local_s.mac.A2,		0x0006, 	0xDC,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A2", 				"MAC address A1:x:A3:A4:A5:A6, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_MAC_A3, 		parameters.list_s.central_s.network_s.local_s.mac.A3,		0x0007, 	0xD4,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A3", 				"MAC address A1:A2:x:A4:A5:A6, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_MAC_A4, 		parameters.list_s.central_s.network_s.local_s.mac.A4,		0x0008, 	0xD2,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A4", 				"MAC address A1:A2:A3:x:A5:A6, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_MAC_A5, 		parameters.list_s.central_s.network_s.local_s.mac.A5,		0x0009, 	0x39,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A5", 				"MAC address A1:A2:A3:A4:x:A6, x=<0;255>. Save and restart system to apply.");
	parameter_initialization(CENTRAL_SYSTEM_MAC_A6, 		parameters.list_s.central_s.network_s.local_s.mac.A6,		0x000A, 	0x16,	variable_uint8_e, level_develop_e,	"network.configuration.MAC.A6", 				"MAC address A1:A2:A3:A4:A5:x, x=<0;255>. Save and restart system to apply.");
	
	//PERIPHERIAL
	parameter_initialization(PERIPHERIAL_SELECT_INTERFACE, 		parameters.list_s.peripheral_s.interface,		0x1001, 	peripheral_interface_disable_e,	variable_select_e, level_develop_e,	"interface.type", 				"{\\\"d\\\":\\\"Interface type\\\",\\\"i\\\":[\\\"Disabled\\\",\\\"ETHERNET\\\",\\\"CAN\\\",\\\"RS485\\\",\\\"RS232\\\"]}" );	
}
