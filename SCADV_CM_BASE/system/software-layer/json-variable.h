#ifndef _JSON_VARIABLE_H_
#define _JSON_VARIABLE_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-27
*	@brief JSON variable
*/

#include "variable.h"

enum json_variable_information_t{
	json_variable_full_e=1,
	json_variable_reduced_e,
	json_variable_only_value_e,
	json_variable_only_access_level_e,
	json_variable_only_alias_e
};

/**
* Give JSON text for pointed variable
* @param[in] pVariable Pointer to variable
* @param[out] JSON_text Pointer to output JSON text
* @param[in] JSON_maximum_length Maximum length of JSON text
* @param[out] JSON_length Length of JSON text
* @param[in] variable_information Switch beetwean full and reduced (id, value) information of variable
*/
void JSON_get_variable(variable_t* pVariable, char* JSON_text, uint16_t JSON_maximum_length, uint16_t* JSON_length, uint8_t variable_information);


#endif
