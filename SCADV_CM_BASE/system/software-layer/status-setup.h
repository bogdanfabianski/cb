#ifndef _STATUS_SETUP_H_
#define _STATUS_SETUP_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-28
*	@brief Status setup
*/

/**
* Initialize status structure
*/
void status_setup(void);

#endif
