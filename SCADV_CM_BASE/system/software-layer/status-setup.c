/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Status
*/
#include "status.h"
#include "status-enum.h"
#include "access-level.h"

#define status_initialization(identifier, status_name, status_id, status_type, status_access_level, status_name_text, status_description_text) \
	status.count++;	\
	static const variable_const_t _setup_status_struct_ ## identifier ## _ = { status_id, status_type, status_name_text, status_description_text }; \
	status_name.settings_s.pConst_s = (variable_const_t* )&_setup_status_struct_ ## identifier ## _ ; \
	status_name.settings_s.var_s.access_level 	= status_access_level;



void status_setup(){
	//Status initialization	
	status.struct_version 	= 0x00001;	
	status.size 			= sizeof(status_t);
	status.count 			= 0;
	
	//CENTRAL
	status_initialization(CENTRAL_TEMPERATURE, 		status.list_s.central_s.temperature,		0x0001, 	variable_float32_e,  level_user_e,	"central.temperature", 					"Main board temperature");
		
	//PERIPHERIAL
	status_initialization(PERIPHERIAL_TEMPERATURE, 	status.list_s.peripheral_s.temperature, 	0x1001, 	variable_float32_e, level_user_e,	"peripheral.temperature", 				"Peripheral board temperature" );	
}
