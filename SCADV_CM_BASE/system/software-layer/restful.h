#ifndef _RESTFUL_H_
#define _RESTFUL_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-28
*	@brief RESTful api
*/

#include "lwip/api.h"

#define HTTP_SEND_HEADER_YES 1
#define HTTP_SEND_HEADER_NO 0


/**
* Get parameter in JSON and send to client
* @param[in] connection Tcp connection handler
* @param[in] id Id of parameter
*/
void restful_get_parameter_by_id(struct netconn *connection, uint32_t id);

/**
* Set parameter value and get JSON and send to client
* @param[in] connection Tcp connection handler
* @param[in] id Id of parameter
* @param[in] value_string String containing value to be set
*/
void restful_set_parameter_value_by_id(struct netconn *connection, uint32_t id, char* value_string);


/**
* Get parameter structure in JSON and send to client
* @param[in] connection Tcp connection handler
* @param[in] variable_information witch beetwean full and reduced (id, value) information of variable
* @param[in] send_header Set 1 to send default http header
*/
void restful_get_parameter_structure(struct netconn *connection, uint8_t variable_information, uint8_t send_header);


/**
* Get status structure in JSON and send to client
* @param[in] connection Tcp connection handler
* @param[in] variable_information witch beetwean full and reduced (id, value) information of variable
* @param[in] send_header Set 1 to send default http header
*/
void restful_get_status_structure(struct netconn *connection, uint8_t variable_information, uint8_t send_header);



#endif
