/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Status
*/
#include "status.h"
#include "variable.h"

status_t status;

/**
 * Get size of status list
 * @return size of status list
 */
uint32_t status_get_size_of_list(void);


status_t* status_find_by_id(uint32_t id){
		
	status_t* pStatus=0;	
	variable_t* pStart = (variable_t*)status_get_fisrt();
	uint32_t end_address = (uint32_t)status_get_fisrt();
	end_address += status_get_size_of_list();
	
	pStatus = (status_t*) variable_find_by_id( id, pStart, end_address);
	
	return pStatus;
}

status_t* status_get_fisrt(void)
{
	return (status_t*) &status.list_s;
}


status_t* status_get_next_status(status_t* pStatus)
{	
	uint32_t end_address = (uint32_t) status_get_fisrt();
	end_address += status_get_size_of_list();
	status_t* pStatus_next=0;
	
	pStatus_next = (status_t*)variable_get_next_variable((variable_t*) pStatus);	
	if((uint32_t)pStatus_next > end_address ) pStatus_next=NULL; // NULL - out of memory range
	
	return pStatus_next;
}


//Private function

uint32_t status_get_size_of_list(void)
{
	return sizeof(status.list_s);
}

