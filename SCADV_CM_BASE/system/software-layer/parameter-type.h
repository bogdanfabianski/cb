#ifndef _PARAMETER_TYPE_H_
#define _PARAMETER_TYPE_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameter type
*/
#include "parameter-type-common.h"
	
//List of parameters stored in NVM
typedef struct{

  uint32_t  struct_version;	///< Parameter version fo structure - changed by programmer
  uint32_t	count;	///< Number of parameters
  uint32_t	size;	///< Size of parameters struct
  
  struct{
	  //Central board
	  struct {			  
		  struct{
			  parameter_bool_t local_name;
		  }configuration_s;		  
		  struct{
			 struct{
				 parameter_ip_address_t ip;
				 parameter_mac_address_t mac;
			 }local_s;
		  }network_s;
	  }central_s;
  
	  //Peripherial board
	   struct {
		  parameter_select_t interface;
	  }peripheral_s;
  }list_s;
  
uint32_t CRC32;	///< CRC32 check code of parameters	
}parameters_t; 	///< Parameters structure

#endif
