#ifndef _PARAMETER_H_
#define _PARAMETER_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameter-type.h"

extern parameters_t parameters;

/**
 * Find parameter by id
 * @param[in] id Id of parameter
 * @return Pointer to parameter. Return 0 if not found.
 */
parameter_t* parameter_find_by_id(uint32_t id);

/**
 * Set pointed parameter value
 * @param[in] id Id of parameter
 * @param[in] value_string String containing value to be set
 * @return Pointer to parameter. Return 0 if not found. 
  */
parameter_t* parameter_set_value_by_id(uint32_t id, char* value_string);


/**
 * Get pointer to first parameter
 * @return Pointer to first parameter
 */
parameter_t* parameter_get_fisrt(void);


/**
 * Get pointer to next parameter
 * @param[in] pParameter Pointer to parameter
 * @return Pointer to next parameter. 0 if end.
 */
parameter_t* parameter_get_next_parameter(parameter_t* pParameter);


#endif
