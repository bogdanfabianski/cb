#ifndef _PARAMETER_TYPE_COMMON_H_
#define _PARAMETER_TYPE_COMMON_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameter type
*/
#include "parameter-type-basic.h"
	
typedef struct{
	parameter_uint8_t A1;
	parameter_uint8_t A2;
	parameter_uint8_t A3;
	parameter_uint8_t A4;
}parameter_ip_address_t;		///< IP address type

typedef struct{
	parameter_uint8_t A1;
	parameter_uint8_t A2;
	parameter_uint8_t A3;
	parameter_uint8_t A4;
	parameter_uint8_t A5;
	parameter_uint8_t A6;
}parameter_mac_address_t;		///< MAC address type


#endif
