/**
*	@author Dominik Luczak
*	@date 2018-08-22
*	@brief Firmware version
*/

#include "firmware-version.h"

const char* firmware_version = "\"Scadvance v1.0\"";
const char* firmware_build_time = "\"" __DATE__ " : " __TIME__ "\"";
