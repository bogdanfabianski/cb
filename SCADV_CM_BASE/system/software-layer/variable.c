/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Variable
*/
#include <stdlib.h>
#include "variable.h"

//Local function declaration
static uint32_t variable_size_of(variable_t* address);


variable_t* variable_find_by_id(uint32_t id, variable_t* pStart, uint32_t pEnd)
{
	variable_t* pVariable = pStart;	
	while(pVariable!=NULL)
	{
		if(pVariable->settings_s.pConst_s->id == id){return pVariable;}//stop seeking variable was found	
		pVariable = variable_get_next_variable(pVariable);
		if((uint32_t)pVariable > pEnd ) pVariable=NULL; // STOP - out of memory range
	}	
	return 0; //Not found
}


void variable_set_value(variable_t* pVariable, char* value_string)
{
	if(pVariable>0){
		if(pVariable->settings_s.pConst_s->type == variable_float32_e)//float32
		{				
			variable_float32_t* pVariable_float32 = (variable_float32_t*)pVariable;
			float32_t set_value=0;
			set_value = atof(value_string);
			pVariable_float32->value = set_value;			
		}
		else if(pVariable->settings_s.pConst_s->type == variable_uint8_e)//uint8
		{
			variable_uint8_t* pVariable_uint8 = (variable_uint8_t*)pVariable;
			uint16_t set_value=0;
			set_value = (uint8_t)atoi(value_string);
			pVariable_uint8->value = set_value;			
		}	
		else if(pVariable->settings_s.pConst_s->type == variable_uint16_e)//uint16
		{
			variable_uint16_t* pVariable_uint16 = (variable_uint16_t*)pVariable;
			uint16_t set_value=0;
			set_value = (uint16_t)atoi(value_string);
			pVariable_uint16->value = set_value;			
		}
		else if(pVariable->settings_s.pConst_s->type == variable_uint32_e)//uint32
		{
			variable_uint32_t* pVariable_uint32 = (variable_uint32_t*)pVariable;
			uint32_t set_value=0;
			set_value = (uint32_t)atol(value_string);
			pVariable_uint32->value = set_value;			
		}	
		else if(pVariable->settings_s.pConst_s->type == variable_select_e)//select
		{
			variable_select_t* pVariable_select = (variable_select_t*)pVariable;
			uint8_t set_value=0;
			set_value = (uint8_t)atoi(value_string);
			pVariable_select->value = set_value;			
		}		
		else	if(pVariable->settings_s.pConst_s->type == variable_bool_e)//bool
		{
			variable_bool_t* pVariable_bool = (variable_bool_t*)pVariable;
			uint8_t set_value=0;
			set_value = (uint8_t)atoi(value_string);
			pVariable_bool->value = set_value==0?0:1;			
		}
	}
}


void variable_set_alias(variable_t* pVariable, char* alias){
	if(pVariable>0){
		strcpy(pVariable->settings_s.var_s.alias, alias);
	}
}


void variable_set_access_level(variable_t* pVariable, uint8_t level)
{
	if(pVariable>0){
		pVariable->settings_s.var_s.access_level = level;
	}
}


/**
 * Get next variable
 * @param[in] pVariable Ponter to current variable in structure
 * @return Pointer to next variable in structure. Return 0 if not found.
 */
variable_t* variable_get_next_variable(variable_t* pVariable)
{
	if(pVariable>0)
	{
		if(pVariable->settings_s.pConst_s!=NULL){
			return (variable_t*)((uint32_t)pVariable+variable_size_of(pVariable)); //get pointer of next element
		}
	}
	return NULL;
}


//Local function definition


/**
 * Size of variable
 * @param[in] pVariable Ponter to variable
 * @return Size of pointed variable. Return 0 if not found.
 */
static uint32_t variable_size_of(variable_t* pVariable)
{
	if(pVariable!=NULL){
		if(pVariable->settings_s.pConst_s!=NULL){
			if(pVariable->settings_s.pConst_s->type==variable_float32_e || pVariable->settings_s.pConst_s->type==variable_uint32_e || pVariable->settings_s.pConst_s->type==variable_int32_e){	//32bit
				return (sizeof(variable_float32_t)); 
			}
			else if(pVariable->settings_s.pConst_s->type==variable_uint16_e || pVariable->settings_s.pConst_s->type==variable_int16_e){//16bit
				return (sizeof(variable_uint16_t)); 
			}
			else if(pVariable->settings_s.pConst_s->type==variable_select_e || pVariable->settings_s.pConst_s->type==variable_bool_e || pVariable->settings_s.pConst_s->type==variable_uint8_e || pVariable->settings_s.pConst_s->type==variable_int8_e){//8bit			
				return (sizeof(variable_uint8_t));
			}
		}
	}
	return 0;
}


