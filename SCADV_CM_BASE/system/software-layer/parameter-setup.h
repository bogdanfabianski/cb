#ifndef _PARAMETER_SETUP_H_
#define _PARAMETER_SETUP_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameter setup
*/

/**
* Initialize paremeters structure
*/
void parameter_setup(void);

#endif
