/**
*	@author Dominik Luczak
*	@date 2018-09-27
*	@brief JSON variable
*/

#include <stdio.h>
#include "json-variable.h"

void JSON_get_variable(variable_t* pVariable, char* JSON_text, uint16_t JSON_maximum_length, uint16_t* JSON_length, uint8_t variable_information)
{	
	if(pVariable){
		if(variable_information == json_variable_only_access_level_e){//only access level
			*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"l\":%u}",pVariable->settings_s.pConst_s->id, pVariable->settings_s.var_s.access_level);
		}
		else	if(variable_information == json_variable_only_alias_e){//only alias
			*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"a\":\"%s\"}",pVariable->settings_s.pConst_s->id, pVariable->settings_s.var_s.alias);
		}else //Full, reduced, only value

		if(pVariable->settings_s.pConst_s->type == variable_uint8_e)//uint8
		{
			variable_uint8_t* pVariable_uint8 = (variable_uint8_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%u", pVariable_uint8->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u,\"t\":\"u\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable_uint8->settings_s.pConst_s->id, pVariable_uint8->value, pVariable_uint8->settings_s.pConst_s->name, pVariable_uint8->settings_s.pConst_s->description, pVariable_uint8->settings_s.var_s.access_level, pVariable_uint8->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u}",pVariable_uint8->settings_s.pConst_s->id, pVariable_uint8->value);
		}		
		else if(pVariable->settings_s.pConst_s->type == variable_uint16_e)//uint16
		{
			variable_uint16_t* pVariable_uint16 = (variable_uint16_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%u", pVariable_uint16->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u,\"t\":\"u\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable_uint16->settings_s.pConst_s->id, pVariable_uint16->value, pVariable_uint16->settings_s.pConst_s->name, pVariable_uint16->settings_s.pConst_s->description, pVariable_uint16->settings_s.var_s.access_level, pVariable_uint16->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u}",pVariable_uint16->settings_s.pConst_s->id, pVariable_uint16->value);
		}
		else if(pVariable->settings_s.pConst_s->type == variable_uint32_e)//uint32
		{			
			variable_uint32_t* pVariable_uint32 = (variable_uint32_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%u", pVariable_uint32->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u,\"t\":\"u\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable_uint32->settings_s.pConst_s->id, pVariable_uint32->value, pVariable_uint32->settings_s.pConst_s->name, pVariable_uint32->settings_s.pConst_s->description, pVariable_uint32->settings_s.var_s.access_level, pVariable_uint32->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u}",pVariable_uint32->settings_s.pConst_s->id, pVariable_uint32->value);
		}
		else if(pVariable->settings_s.pConst_s->type == variable_float32_e)//float32
		{				
			variable_float32_t* pVariable_float32 = (variable_float32_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%g", pVariable_float32->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%g,\"t\":\"f\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable->settings_s.pConst_s->id, pVariable_float32->value, pVariable_float32->settings_s.pConst_s->name, pVariable_float32->settings_s.pConst_s->description, pVariable_float32->settings_s.var_s.access_level, pVariable_float32->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%g}",pVariable->settings_s.pConst_s->id, pVariable_float32->value);				
		}		
		else if(pVariable->settings_s.pConst_s->type == variable_bool_e)//bool
		{
			variable_bool_t* pVariable_bool = (variable_bool_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%u", pVariable_bool->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u,\"t\":\"b\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable_bool->settings_s.pConst_s->id, pVariable_bool->value, pVariable_bool->settings_s.pConst_s->name, pVariable_bool->settings_s.pConst_s->description, pVariable_bool->settings_s.var_s.access_level, pVariable_bool->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u}",pVariable_bool->settings_s.pConst_s->id, pVariable_bool->value);
		}		
		else if(pVariable->settings_s.pConst_s->type == variable_select_e)//select
		{
			variable_select_t* pVariable_select = (variable_select_t*)pVariable;
			if(variable_information == json_variable_only_value_e){
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"%u", pVariable_select->value);
			}else if(variable_information == json_variable_full_e)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u,\"t\":\"s\",\"n\":\"%s\",\"d\":\"%s\",\"l\":%d,\"a\":\"%s\"}",pVariable_select->settings_s.pConst_s->id, pVariable_select->value, pVariable_select->settings_s.pConst_s->name, pVariable_select->settings_s.pConst_s->description, pVariable_select->settings_s.var_s.access_level, pVariable_select->settings_s.var_s.alias);
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"%04X\",\"v\":%u}",pVariable_select->settings_s.pConst_s->id, pVariable_select->value);
		}		
		else{//Variable unknown
			if(variable_information)
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"0\",\"v\":0,\"t\":\"n\",\"n\":\"unknown\",\"d\":\"unknown\"}");		
			else
				*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"0\",\"v\":0}");		
		}

	}else{//Variable don't exists
		if(variable_information)
			*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"0\",\"v\":0,\"t\":\"n\",\"n\":\"noname\",\"d\":\"unknown\"}");		
		else
			*JSON_length=snprintf(JSON_text,JSON_maximum_length,"{\"id\":\"0\",\"v\":0}");		
	}		
}
