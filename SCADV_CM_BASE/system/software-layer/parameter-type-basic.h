#ifndef _PARAMETER_TYPE_BASIC_H_
#define _PARAMETER_TYPE_BASIC_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameter type
*/
#include "variable-type-basic.h"
	
//Default type of parameter
typedef variable_t parameter_t;

typedef variable_uint8_t 	parameter_uint8_t;
typedef variable_uint16_t 	parameter_uint16_t;
typedef variable_uint32_t 	parameter_uint32_t;
typedef variable_float32_t 	parameter_float32_t;
typedef variable_bool_t 	parameter_bool_t;
typedef variable_select_t 	parameter_select_t;
	

#endif
