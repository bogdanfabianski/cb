#ifndef _ACCESS_LEVEL_H_
#define _ACCESS_LEVEL_H_
/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Access level of system parameters and satus.
*/
enum access_level_t{
	level_develop_e=0,
	level_expert_e=2,
	level_user_e=4
};
#endif
