/**
*	@author Dominik Luczak
*	@date 2018-09-28
*	@brief RESTful api
*/

#include "restful.h"
#include "json-variable.h"
#include "parameter.h"
#include "status.h"

#define HTTP_HEADER "HTTP/1.0 200 OK\r\nServer: Scadvance-D-Luczak\r\nAccess-Control-Allow-Origin: *\r\nContent-type: text/html\r\n\r\n"
#define HTTP_HEADER_LENGTH (sizeof(HTTP_HEADER)-1)

#define RESTFUL_MAXIMUM_RESPONSE_LENGTH 1000

void restful_get_parameter_by_id(struct netconn *connection, uint32_t id)
{  
	uint16_t response_length=0;
	char restful_json_response[RESTFUL_MAXIMUM_RESPONSE_LENGTH];

	//Find parameter
	parameter_t* pParametereter = parameter_find_by_id(id);
	//Prepare JSON text
	JSON_get_variable( (variable_t*) pParametereter, restful_json_response, RESTFUL_MAXIMUM_RESPONSE_LENGTH, &response_length, json_variable_reduced_e); //JSON text

	//Send HTTP header
	netconn_write(connection, HTTP_HEADER, HTTP_HEADER_LENGTH, NETCONN_NOCOPY);
	//Send JSON text
	netconn_write(connection, restful_json_response, response_length, NETCONN_COPY);
}

void restful_set_parameter_value_by_id(struct netconn *connection, uint32_t id, char* value_string)
{
	uint16_t response_length=0;
	char restful_json_response[RESTFUL_MAXIMUM_RESPONSE_LENGTH];
	
	//Find and set parameter value
	parameter_t* pParametereter = parameter_set_value_by_id(id, value_string);	
	//Prepare JSON text
	JSON_get_variable( (variable_t*) pParametereter, restful_json_response, RESTFUL_MAXIMUM_RESPONSE_LENGTH, &response_length, json_variable_reduced_e); //JSON text

	//Send HTTP header
	netconn_write(connection, HTTP_HEADER, HTTP_HEADER_LENGTH, NETCONN_NOCOPY);
	//Send JSON text
	netconn_write(connection, restful_json_response, response_length, NETCONN_COPY);	
}


void restful_get_parameter_structure(struct netconn *connection, uint8_t variable_information, uint8_t send_header)
{
	uint16_t response_length=0;
	uint16_t response_current_length=0;
	uint16_t count_parameter=0;
	char restful_json_response[RESTFUL_MAXIMUM_RESPONSE_LENGTH]={0};
	parameter_t* pParameter=0;

	//Send http header
	if(send_header)
	netconn_write(connection,HTTP_HEADER, HTTP_HEADER_LENGTH, NETCONN_NOCOPY);		
		
	//JSON array start
	response_current_length+=sprintf(restful_json_response+response_current_length,"[");
 
	//Send all parameters
	pParameter = parameter_get_fisrt();
	
	while(pParameter!=NULL && count_parameter<parameters.count)
	{		
		JSON_get_variable( (variable_t*) pParameter, restful_json_response+response_current_length, RESTFUL_MAXIMUM_RESPONSE_LENGTH, &response_length, json_variable_full_e); //JSON text
		
		pParameter = parameter_get_next_parameter(pParameter);
		count_parameter++;
		
		//Add separator, except last
		if(pParameter!=NULL && count_parameter!=parameters.count){
			response_length++;
			strcat(restful_json_response, ",");
		}		
		response_current_length += response_length;
		
		//Send current part of response
		if(response_current_length > RESTFUL_MAXIMUM_RESPONSE_LENGTH-300)
		{
			netconn_write(connection, restful_json_response, response_current_length, NETCONN_COPY);	//send data	
			response_current_length=0;
		}
	}	
	
	//Send last part of response if exists
	if(response_current_length>0)
	{
		netconn_write(connection, restful_json_response, response_current_length, NETCONN_COPY);	//send data	
		response_current_length=0;
	}
	
	//JSON array end
	response_length=sprintf(restful_json_response,"]");		
	netconn_write(connection, restful_json_response, response_length, NETCONN_COPY); //send data
}


void restful_get_status_structure(struct netconn *connection, uint8_t variable_information, uint8_t send_header)
{
	uint16_t response_length=0;
	uint16_t response_current_length=0;
	uint16_t count_status=0;
	char restful_json_response[RESTFUL_MAXIMUM_RESPONSE_LENGTH]={0};
	status_t* pStatus=0;

	//Send http header
	if(send_header)
	netconn_write(connection,HTTP_HEADER, HTTP_HEADER_LENGTH, NETCONN_NOCOPY);		
		
	//JSON array start
	response_current_length+=sprintf(restful_json_response+response_current_length,"[");
 
	//Send all parameters
	pStatus = status_get_fisrt();
	
	while(pStatus!=NULL && count_status<status.count)
	{		
		JSON_get_variable( (variable_t*) pStatus, restful_json_response+response_current_length, RESTFUL_MAXIMUM_RESPONSE_LENGTH, &response_length, json_variable_full_e); //JSON text
		
		pStatus = status_get_next_status(pStatus);
		count_status++;
		
		//Add separator, except last
		if(pStatus!=NULL && count_status!=status.count){
			response_length++;
			strcat(restful_json_response, ",");
		}		
		response_current_length += response_length;
		
		//Send current part of response
		if(response_current_length > RESTFUL_MAXIMUM_RESPONSE_LENGTH-300)
		{
			netconn_write(connection, restful_json_response, response_current_length, NETCONN_COPY);	//send data	
			response_current_length=0;
		}
	}	
	
	//Send last part of response if exists
	if(response_current_length>0)
	{
		netconn_write(connection, restful_json_response, response_current_length, NETCONN_COPY);	//send data	
		response_current_length=0;
	}
	
	//JSON array end
	response_length=sprintf(restful_json_response,"]");		
	netconn_write(connection, restful_json_response, response_length, NETCONN_COPY); //send data
}


