#ifndef _FIRMWARE_VERSION_H_
#define _FIRMWARE_VERSION_H_
/**
*	@author Dominik Luczak
*	@date 2018-08-22
*	@brief Firmware version
*/

extern const char* firmware_version;	///< Firmware version. Changed by programmer.
extern const char* firmware_build_time; ///< Firmware build time. Changes automatically.

#endif
