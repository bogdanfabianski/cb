#ifndef _UNIQUE_ID_H_
#define _UNIQUE_ID_H_
/**
*	@author Dominik Luczak
*	@date 2018-07-21
*	@brief Unique 96-bit ID of microcontroller
*/

#include <stdint.h>

#define UNIQUE_ID_TEXT_LENGTH 27

typedef struct
{
 uint32_t ID0;
 uint32_t ID1;
 uint32_t ID2;		
} UID_t;	///< Unique ID structure


extern const UID_t* unique_id_s;										///< Unique ID raw format
extern char unique_id_text[UNIQUE_ID_TEXT_LENGTH];	///< Unique ID text hexadecimal format

/**
* Read hardware uniqued ID.
* @brief Readed ID is written to volitail memory in raw and text format. Function should be execiuted onced in initialization part.
*/
void unique_id_read(void);

#endif
