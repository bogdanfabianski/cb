/**
*	@author Dominik Luczak
*	@date 2018-07-22
*	@brief Unique 96-bit ID of microcontroller
*/

#include <string.h>
#include <stdio.h>
#include "unique-id.h"

#define UNIQUE_ID_ADDRESS 0x1FF1E800 //read from en.DM00314099.pdf

const UID_t* unique_id_s = (UID_t*)UNIQUE_ID_ADDRESS;			
char unique_id_text[UNIQUE_ID_TEXT_LENGTH]="";	

void unique_id_read(void){
	sprintf(unique_id_text,"\"%08X%08X%08X\"",unique_id_s->ID0, unique_id_s->ID1, unique_id_s->ID2);
}
