/**
  ******************************************************************************
  * File Name          : websocket.h
  * Description        : This file provides code for the configuration
  *                      of the mbedtls instances.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __websocket_H
#define __websocket_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stddef.h>
#include "stm32h7xx_hal.h"
#include <string.h>


#include "lwip/opt.h"
#include "lwip/ip.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/icmp.h"
#include "fs.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "lan8742.h"
#include "net_layer.h"
#include "mbedtls.h"

/* USER CODE BEGIN 0 */
/* MAC ADDRESS*/

/* USER CODE END 0 */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN 1 */
/* USER CODE END 1 */

/* MBEDTLS init function */

/* USER CODE BEGIN 2 */
err_t websocket_send(struct netconn *conn, const void *dataptr, uint8_t opcode, uint8_t Mask, uint32_t MaskKey, uint32_t len);
err_t websocket_recv(struct netconn *conn, const void *dataptr, uint32_t *len);
void websocket_net_thread(void * pvParameters);
struct netconn* websocket_connect(err_t *err, ip_addr_t* local_ipaddr, ip_addr_t* remote_ipaddr, uint16_t remote_port, uint16_t local_port, char *localkey, char *remotekey);
/* USER CODE END 2 */

#ifdef __cplusplus
}
#endif
#endif /*__websocket_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Poznan University of Technology *****END OF FILE****/
