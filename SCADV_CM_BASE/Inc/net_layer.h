/**
  ******************************************************************************
  * File Name          : net_layer.h
  * Description        : This file provides code for the network layer
  *                      of the probe.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __net_layer_H
#define __net_layer_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN 0 */
#include <stddef.h>
#include "stm32h7xx_hal.h"
#include <string.h>


#include "lwip/opt.h"
#include "lwip/ip.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/icmp.h"
#include "fs.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "lan8742.h"
/* MAC ADDRESS*/

#define MAC_ADDR0   0x8C
#define MAC_ADDR1   0xDC
#define MAC_ADDR2   0xD4
	
#define MAC_ADDR3   0xD2
#define MAC_ADDR4   0x39
#define MAC_ADDR5   0x16
 
/*Static IP ADDRESS*/
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   34
#define IP_ADDR3   21

#define SERVER_IP_ADDR0   192
#define SERVER_IP_ADDR1   168
#define SERVER_IP_ADDR2   34
#define SERVER_IP_ADDR3   22

/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0

/*Gateway Address*/
#define GW_ADDR0   192
#define GW_ADDR1   168
#define GW_ADDR2   34
#define GW_ADDR3   1 

/* USER CODE END 0 */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN 1 */
/* USER CODE END 1 */

/* MBEDTLS init function */

/* USER CODE BEGIN 2 */
err_t net_recv(struct netconn *conn, void *dataptr, uint16_t *len);
err_t net_send(struct netconn *conn, const void *dataptr, uint32_t size, uint8_t apiflags);
struct netconn* net_connect(ip_addr_t* local_ipaddr, ip_addr_t* remote_ipaddr, uint16_t remote_port, uint16_t local_port);

/* USER CODE END 2 */

#ifdef __cplusplus
}
#endif
#endif /*__net_layer_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT Poznan University of Technology *****END OF FILE****/
