#ifndef __HTTPSERVER_NETCONN_H__
#define __HTTPSERVER_NETCONN_H__

//#include "stm324xg_eval.h"
#include "main.h"
#include "lwip/opt.h"
#include "main.h"
#if LWIP_DHCP
#include "lwip/dhcp.h"
#endif
#include "ethernetif.h"


/* MAC ADDRESS*/

#define MAC_ADDR0   0x8C//0x45
#define MAC_ADDR1   0xDC//0x58
#define MAC_ADDR2   0xD4//0x50
	
#define MAC_ADDR3   0xD2//0x00
#define MAC_ADDR4   0x39///0x00
#define MAC_ADDR5   0x16//0x01
 
/*Static IP ADDRESS*/
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   34
#define IP_ADDR3   21

#define SERVER_IP_ADDR0   192//172//192//172
#define SERVER_IP_ADDR1   168//16///168//16
#define SERVER_IP_ADDR2   34//17//34//17
#define SERVER_IP_ADDR3   22//101//20//101

/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0

/*Gateway Address*/
#define GW_ADDR0   192
#define GW_ADDR1   168
#define GW_ADDR2   34
#define GW_ADDR3   1 

//typedef enum protocol_id_

typedef struct probe_ethernet_field {
	uint32_t probe_id;
	uint32_t time_stamp[2];
	uint16_t protocol_id;
	uint8_t  src_dst_type;
	uint8_t  from_id[6];
	uint8_t  to_id[6];
	uint8_t* data;
} probe_ethernet_field_td;




struct netconn_MTS_def
{
int iMessageLength;
char* pvMessage;
};


typedef struct netconn_MTS_def netconn_MTS;

struct netconn_MTSS_def
{
int iMessages;
netconn_MTS* pnNetconnMessage;
};
typedef struct netconn_MTSS_def netconn_MTSS;


void http_netconn_thread(void *arg);
void http_netconn_thread_s(void *arg);
void xml_client_netconn_init(void);
void xml_client_sender_netconn_thread(void *arg);
void sntp_netconn_thread(void *arg);
void icmp_netconn_thread(void *arg);
void ethernet_link_status_updated(struct netif *netif);
uint8_t* getRSSIBuffer(void);
//void DynWebPage(struct netconn *conn);

#endif /* __HTTPSERVER_NETCONN_H__ */
