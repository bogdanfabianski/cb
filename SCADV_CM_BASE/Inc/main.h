/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define SPI4_DAT2_CLK_Pin GPIO_PIN_2
#define SPI4_DAT2_CLK_GPIO_Port GPIOE
#define SPI4_DAT2_CS_Pin GPIO_PIN_4
#define SPI4_DAT2_CS_GPIO_Port GPIOE
#define SPI4_DAT2_MOSI_Pin GPIO_PIN_6
#define SPI4_DAT2_MOSI_GPIO_Port GPIOE
#define SPI5_CMD_CSA_Pin GPIO_PIN_6
#define SPI5_CMD_CSA_GPIO_Port GPIOF
#define SPI5_CMD_CLK_Pin GPIO_PIN_7
#define SPI5_CMD_CLK_GPIO_Port GPIOF
#define SPI5_CMD_MISO_Pin GPIO_PIN_8
#define SPI5_CMD_MISO_GPIO_Port GPIOF
#define SPI5_CMD_MOSI_Pin GPIO_PIN_9
#define SPI5_CMD_MOSI_GPIO_Port GPIOF
#define SPI5_CMD_CSB_Pin GPIO_PIN_10
#define SPI5_CMD_CSB_GPIO_Port GPIOF
#define SPI1_DAT1_CS_Pin GPIO_PIN_4
#define SPI1_DAT1_CS_GPIO_Port GPIOA
#define SPI1_DAT1_SCK_Pin GPIO_PIN_5
#define SPI1_DAT1_SCK_GPIO_Port GPIOA
#define ASIG_IN_VBAT_M_Pin GPIO_PIN_0
#define ASIG_IN_VBAT_M_GPIO_Port GPIOB
#define ASIG_IN_3V3_M_Pin GPIO_PIN_1
#define ASIG_IN_3V3_M_GPIO_Port GPIOB
#define DSIG_OUT_RGB_CMM_Pin GPIO_PIN_14
#define DSIG_OUT_RGB_CMM_GPIO_Port GPIOB
#define DSIG_OUT_RGB_HB_Pin GPIO_PIN_15
#define DSIG_OUT_RGB_HB_GPIO_Port GPIOB
#define DSIG_OUT_RGB_ETH_ACT_Pin GPIO_PIN_11
#define DSIG_OUT_RGB_ETH_ACT_GPIO_Port GPIOD
#define DSIG_OUT_BOOT0_CTRL_Pin GPIO_PIN_12
#define DSIG_OUT_BOOT0_CTRL_GPIO_Port GPIOD
#define DSIG_IN_BTN_WUP_Pin GPIO_PIN_13
#define DSIG_IN_BTN_WUP_GPIO_Port GPIOD
#define SDMMC1_D0_Pin GPIO_PIN_8
#define SDMMC1_D0_GPIO_Port GPIOC
#define SDMMC1_D1_Pin GPIO_PIN_9
#define SDMMC1_D1_GPIO_Port GPIOC
#define SDMMC1_D2_Pin GPIO_PIN_10
#define SDMMC1_D2_GPIO_Port GPIOC
#define SDMMC1_D3_Pin GPIO_PIN_11
#define SDMMC1_D3_GPIO_Port GPIOC
#define SDMMC1_CLK_Pin GPIO_PIN_12
#define SDMMC1_CLK_GPIO_Port GPIOC
#define SDMMC1_CMD_Pin GPIO_PIN_2
#define SDMMC1_CMD_GPIO_Port GPIOD
#define SPI1_DAT1_MOSI_Pin GPIO_PIN_7
#define SPI1_DAT1_MOSI_GPIO_Port GPIOD
#define DSIG_IN_CHR_STAT2_Pin GPIO_PIN_9
#define DSIG_IN_CHR_STAT2_GPIO_Port GPIOG
#define DSIG_IN_CHR_STAT1_Pin GPIO_PIN_10
#define DSIG_IN_CHR_STAT1_GPIO_Port GPIOG
#define DSIG_IN_CHR_PG_Pin GPIO_PIN_11
#define DSIG_IN_CHR_PG_GPIO_Port GPIOG
#define DSIG_OUT_EN_VBAT_Pin GPIO_PIN_3
#define DSIG_OUT_EN_VBAT_GPIO_Port GPIOB
#define DSIG_OUT_5VD_CHR_EN_Pin GPIO_PIN_4
#define DSIG_OUT_5VD_CHR_EN_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define DSIG_RGB_A_BLUE(z)							 				(z ? (DSIG_OUT_RGB_CMM_GPIO_Port->BSRRL = DSIG_OUT_RGB_CMM_Pin) : (DSIG_OUT_RGB_CMM_GPIO_Port->BSRRH = DSIG_OUT_RGB_CMM_Pin))
#define DSIG_RGB_A_BLUE_TOGGLE									DSIG_OUT_RGB_CMM_GPIO_Port->ODR^=(DSIG_OUT_RGB_CMM_Pin)

#define DSIG_RGB_A_GREEN(z)							 				(z ? (DSIG_OUT_RGB_ETH_ACT_GPIO_Port->BSRRL = DSIG_OUT_RGB_ETH_ACT_Pin) : (DSIG_OUT_RGB_ETH_ACT_GPIO_Port->BSRRH = DSIG_OUT_RGB_ETH_ACT_Pin))
#define DSIG_RGB_A_GREEN_TOGGLE									DSIG_OUT_RGB_ETH_ACT_GPIO_Port->ODR^=(DSIG_OUT_RGB_ETH_ACT_Pin)

#define DSIG_RGB_A_RED(z)							 				(z ? (DSIG_OUT_RGB_HB_GPIO_Port->BSRRL = DSIG_OUT_RGB_HB_Pin) : (DSIG_OUT_RGB_HB_GPIO_Port->BSRRH = DSIG_OUT_RGB_HB_Pin))
#define DSIG_RGB_A_RED_TOGGLE									DSIG_OUT_RGB_HB_GPIO_Port->ODR^=(DSIG_OUT_RGB_HB_Pin)

#define DSIG_5V_EN(z)							 				(z ? (DSIG_OUT_5VD_CHR_EN_GPIO_Port->BSRRL = DSIG_OUT_5VD_CHR_EN_Pin) : (DSIG_OUT_5VD_CHR_EN_GPIO_Port->BSRRH = DSIG_OUT_5VD_CHR_EN_Pin))
#define DSIG_5V_EN_TOGGLE								  DSIG_OUT_5VD_CHR_EN_GPIO_Port->ODR^=(DSIG_OUT_5VD_CHR_EN_Pin)

#define DSIG_LAN_nRST(z)  (z ? (GPIOA->BSRRL = GPIO_PIN_3) : (GPIOA->BSRRH = GPIO_PIN_3))


#include "SEGGER_RTT.h"

extern char log_string[205];


#define RTT_LOG(...) {\
	snprintf(log_string, 200, __VA_ARGS__); \
	strcat(log_string,"\n");\
	SEGGER_RTT_WriteString(0,log_string);\
}

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
