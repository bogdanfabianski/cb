/**
  ******************************************************************************
  * File Name          : mbedtls.h
  * Description        : This file provides code for the configuration
  *                      of the mbedtls instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __mbedtls_H
#define __mbedtls_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "mbedtls/config.h"

/* USER CODE BEGIN 0 */
#include <stddef.h>
#include "stm32h7xx_hal.h"
#include <string.h>

#include "lwip/opt.h"
#include "lwip/ip.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/icmp.h"
#include "fs.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "lan8742.h"
#include "net_layer.h"



#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"

#include "mbedtls/net_sockets.h"
/* USER CODE END 0 */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN 1 */


typedef enum
{
	/** No error, everything OK.	*/
  MBEDTLS_OK         = 0,
/** Out of memory error.   			*/
  MBEDTLS_x509_crt  = -1,

}mbedtls_err;
/* USER CODE END 1 */

/* MBEDTLS init function */
mbedtls_err MX_MBEDTLS_Init(mbedtls_pk_context *ctx, mbedtls_entropy_context *entropy,	mbedtls_ctr_drbg_context *ctr_drbg, mbedtls_ssl_context *ssl,
															mbedtls_ssl_config *conf,	mbedtls_x509_crt *cacert, const char *SERVER_NAME);

/* USER CODE BEGIN 2 */
struct netconn* mbedtls_net_connect2(ip_addr_t* local_ipaddr, ip_addr_t* remote_ipaddr, uint16_t remote_port, uint16_t local_port,
																		mbedtls_ssl_config *conf, mbedtls_ssl_context *ssl,const char *SERVER_NAME, void *server_fd);
int mbedtls_net_send2( void *ctx, const unsigned char *buf, size_t len );
int mbedtls_net_recv2( void *ctx, unsigned char *buf, size_t len );
int mbedtls_hardware_poll( void *Data, unsigned char *Output, size_t Len, size_t *oLen );
void mbedtls_net_thread(void * pvParameters);
/* USER CODE END 2 */

#ifdef __cplusplus
}
#endif
#endif /*__mbedtls_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
